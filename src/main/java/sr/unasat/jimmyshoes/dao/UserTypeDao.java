package sr.unasat.jimmyshoes.dao;

import sr.unasat.jimmyshoes.entities.User;
import sr.unasat.jimmyshoes.entities.UserType;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserTypeDao {

    private EntityManager entityManager;

    public UserTypeDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<UserType> retrieveUserTypeList() {
        entityManager.getTransaction().begin();
        String jpql = "select s from UserType s";
        TypedQuery<UserType> query = entityManager.createQuery(jpql, UserType.class);
        List<UserType> userTypeList = query.getResultList();
        entityManager.getTransaction().commit();
        return userTypeList;
    }

    public UserType retrieveUserTypebyName(String name) {
        entityManager.getTransaction().begin();
        String jpql = "select t from UserType t where t.name = :name";
        TypedQuery<UserType> query = entityManager.createQuery(jpql, UserType.class);
        UserType userType = query.setParameter("name", name).getSingleResult();
        entityManager.getTransaction().commit();
        return userType;
    }

    public UserType insertUserType(UserType userType) {
        entityManager.getTransaction().begin();
        entityManager.persist(userType);
        entityManager.getTransaction().commit();
        return userType;
    }

    public int updateUserType(UserType userType) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("UPDATE UserType p SET p.name = :name where p.id = :id");
        query.setParameter("id", userType.getUser_type_id());
        query.setParameter("name", userType.getName());
        int rowsUpdated = query.executeUpdate();
        System.out.println("entities Updated: " + rowsUpdated);
        entityManager.getTransaction().commit();
        return rowsUpdated;
    }

    public int deleteUsertype(String userTypeName) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM UserType t WHERE  t.name = :userTypeName");
        query.setParameter("userTypeName", userTypeName);
        int rowsDeleted = query.executeUpdate();
        System.out.println("entities deleted: " + rowsDeleted);
        entityManager.getTransaction().commit();
        return rowsDeleted;
    }
}