package sr.unasat.jimmyshoes.application;

import sr.unasat.jimmyshoes.config.JPAConfiguration;
import sr.unasat.jimmyshoes.entities.User;
import sr.unasat.jimmyshoes.entities.UserType;
import sr.unasat.jimmyshoes.services.DataManipulation;

public class Application {

    public static void main(String[] args) {
        System.out.println("Hi");
        JPAConfiguration jpaConfiguration = new JPAConfiguration();
        User user = new User();

//        UserType userType = new UserType("Manager");
        DataManipulation dataManipulation = new DataManipulation();
//        dataManipulation.insertData(userType);
        dataManipulation.insertDefaultUserTypes();
    }
}
