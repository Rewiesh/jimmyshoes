package sr.unasat.jimmyshoes.services;

import sr.unasat.jimmyshoes.config.JPAConfiguration;
import sr.unasat.jimmyshoes.dao.UserDao;
import sr.unasat.jimmyshoes.dao.UserTypeDao;
import sr.unasat.jimmyshoes.entities.UserType;

import java.util.ArrayList;
import java.util.List;

public class DataManipulation {

    UserDao userDao = new UserDao(JPAConfiguration.getEntityManager());
    UserTypeDao userTypeDao = new UserTypeDao(JPAConfiguration.getEntityManager());

    public <T> List<T> getListOfData(String listOf) {
        
        if(listOf == "UserType") {
            return (List<T>) userTypeDao.retrieveUserTypeList();
        }
        else if(listOf == "User"){
            return null;
        }
        else {
            return null;
        }
    }

    public <T> void insertData(T data) {
        if(data instanceof UserType) {
            userTypeDao.insertUserType((UserType) data);
        }
    }

    public <T> void updateData(T data) {
        if(data instanceof UserType) {
            userTypeDao.updateUserType((UserType) data);
        }
    }

    public <T> void deleteData(T data) {
        if(data instanceof UserType){
            userTypeDao.deleteUsertype(((UserType) data).getName());
        }
    }

    public void insertDefaultUserTypes() {
        List<UserType> userTypeList = new ArrayList<>(2);
        UserType admin = new UserType("Admin");
        userTypeList.add(0, admin);
        UserType  employee= new UserType("Employee");
        userTypeList.add(0, employee);
        System.out.println(userTypeList.size());
        for (int i = 0; i < userTypeList.size(); i++) {
            insertData(userTypeList.get(i));
        }
    }

}
